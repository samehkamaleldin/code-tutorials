;-----------------------------------------------------------------------------------
; 1 - example of simple operations
;-----------------------------------------------------------------------------------
; arithmatic operations
(+ 1 2)
(* 2 3)
(/ 8 2)
(> 4 3)
(= 4 6)

; conditions
(if true  "true" "false")
(if false "true" "false")
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 2 - example of using java functions
;-----------------------------------------------------------------------------------
; using function with members of Int class
(.toString 2)

; using function with members of String class
(.length "I am text!")

; create new object of Random class
(new java.util.Random)

; create new object of Random class and save it in a variable
(def rnd (new java.util.Random))

; call a method inside the object instance using (.)
(. rnd nextInt)

; call a method inside the object instance using (.) with passing arguments
(. rnd nextInt 10)
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 3 - example of nested defined functions and call example
;-----------------------------------------------------------------------------------
; define a function that uses if and else condition
(defn get-tax-ratio [salary] (if (> salary 1000) 0.35 0.1 ))

; define a function that uses another defined function inside it
(defn get-tax-value [salary] (* (get-tax-ratio salary) salary))

; cal a function with a parameter
(get-tax-value 1000)
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 4 - example of creating  maps
;-----------------------------------------------------------------------------------
; defining a new map
(def inventors {"Lisp" "McCarthy", "Clojure" "Hickey"})

;looking up for value with the map key
(inventors "Lisp"   )
(inventors "Clojure")

;looking up for map values in the map with default value for nil results
(get inventors "Lisp" "Not Found 404!")
(get inventors "C++"  "Not Found 404!")
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 5 - example of using keyword
;-----------------------------------------------------------------------------------
; define a map using keywords
(def school {:field "Computer Science" :place "Cairo"})

; get value from a data structure using keywords as a function that look up itself
(:field school)

;get a value of keyword using simple look up by the map key
(school :field)
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 6 - example of creating records
;-----------------------------------------------------------------------------------
; defining a new record structure called book with keywords [title author field]
(defrecord Book [title author field])

; defining a new instance of the book record
(def binstance (->Book "AlgorithmsBook" "Al-Khawarzmi" "Mathematics"))

; get the value of the book instance
binstance

; getting the keywords values if the instance b-instance of the book record
(:title  binstance)
(:author binstance)
(:field  binstance)

; create an instance of Book record on the fly
(Book. "Networks" "Alan" "Information Technology")
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 7 - example of using let binding
;-----------------------------------------------------------------------------------
; bind sequence to the relative variables [x <- 1 , y <- 2] and 3 is not used
(let [[x y] [1 2 3]]
  (+ x y)
)

; bind with let and use the '_' neglect character which also take right most value
(let [[_ _ y] [1 7 3]]
  (+ _ y)
)
;***********************************************************************************



;-----------------------------------------------------------------------------------
; 8 - example of using for binding
;-----------------------------------------------------------------------------------
; using simple for binding for a var 'x' as '[3 4 6]'
(for [x [3 4 6]] ; the binding part 
  (* x 2)        ; the body part    s
)

; use for binding for two vars 'x' 'y' as ranges and eval all possible combinations
(for [x (range 1 4) y (range 2 5)]  ; the binding part 
  (+ x y)                           ; the body part    
)
;***********************************************************************************