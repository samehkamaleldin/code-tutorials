; clojure gui demo for sum two numbers using java swing 

(import '(javax.swing JFrame JLabel JTextField JButton)
        '(java.awt.event ActionListener)
        '(java.awt GridLayout))

(defn sum-two-num-gui-demo []
  (let 
    ; =================================================================
    ; Binding Header - Components declaration
    ; =================================================================
    [ frame            (JFrame.     "Sum two numbers"  )
         
      num1-text        (JTextField.                    )
      num2-text        (JTextField.                    )
         
      first-label      (JLabel.     "First num"        )
      second-label     (JLabel.     "Second num"       )
      
      sum-button       (JButton.    "Sum"              )
    ]
    
    ; =================================================================
    ; Binding Body
    ; =================================================================
    
    ; -----------------------------------------------------------------    
    ; add action event to the sum-button
    (.addActionListener sum-button
      (reify ActionListener
            (actionPerformed
             [_ evt]
             (let [
                     num1 (Double/parseDouble (.getText num1-text)),
                     num2 (Double/parseDouble (.getText num2-text))
                  ]
               
               (javax.swing.JOptionPane/showMessageDialog nil (str "Result:" (+ num1 num2)))
             )
            )
      )
    )
    
    ; -----------------------------------------------------------------
    ; configure num1-text properties
    (doto num1-text
      (.setBounds 100 15 200 25)
    )
    
    ; -----------------------------------------------------------------
    ; configure num1-text properties
    (doto num2-text
      (.setBounds 100 45 200 25)
    )
    
    ; -----------------------------------------------------------------
    ; configure sum-buton properties
    (doto sum-button
      (.setBounds 100 80 200 30)
    )
    
    ; -----------------------------------------------------------------
    ; configure first-label properties
    (doto first-label
      (.setBounds 10 10 100 25)
    )
    ; -----------------------------------------------------------------
    ; configure second-label properties
    (doto second-label
      (.setBounds 10 45 100 25) 
    )
    
    ; -----------------------------------------------------------------
    ; configure the frame properties
    (doto frame      
      (.setLayout    nil      )
      
      ; add gui component to frame object
      (.add num1-text    )
      (.add num2-text    )
      (.add first-label  )
      (.add second-label )      
      (.add sum-button   )      
        
      ; adjust frame properties
      (.setSize      350 150  )
      (.setResizable false    )
      (.setVisible   true     )      
      
    )
    ; -----------------------------------------------------------------
  )
)

(sum-two-num-gui-demo)